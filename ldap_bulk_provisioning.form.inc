<?php
/**
 * @file
 * Provides forms used in ldap_bulk_provisioning module.
 */

/**
 * Menu callback for admin/config/people/ldap/user/bulk_d2l.
 */
function ldap_bulk_provisioning_drupal_to_ldap_form($form, &$form_state) {
  $form = array();

  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#required' => TRUE,
    '#options' => user_roles(TRUE),
    '#description' => t('Users which are in at least one of the selected roles will be provisioned (except uid = 0).'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Run provisioning'),
  );

  return $form;
}

/**
 * Submit Callback for drupal_to_ldap_form.
 */
function ldap_bulk_provisioning_drupal_to_ldap_form_submit($form, &$form_state) {
  $roles = array();
  foreach ($form_state['values']['roles'] as $rid => $selected) {
    if ($selected) {
      $roles[] = $rid;
    }
  }

  $batch = array(
    'operations' => array(
      array('_ldap_bulk_provisioning_drupal_to_ldap_operation', array($roles)),
    ),
    'finished' => '_ldap_bulk_provisioning_drupal_to_ldap_finished',
    'title' => t('Processing Bulk Provisioning'),
    'init_message' => t('Bulk provisioning is starting.'),
    'progress_message' => t('Provisioned @current out of @total users.'),
    'error_message' => t('Bulk provisioning has encountered an error.'),
    'file' => drupal_get_path('module', 'ldap_bulk_provisioning') . '/ldap_bulk_provisioning.batch.inc',
  );

  batch_set($batch);
  batch_process('admin/config/people/ldap/user');
}
