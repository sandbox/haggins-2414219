<?php
/**
 * @file
 * Handles batch operations of ldap_bulk_provisioning module.
 */

/**
 * Batch Operation.
 */
function _ldap_bulk_provisioning_drupal_to_ldap_operation($roles, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    if (in_array(DRUPAL_AUTHENTICATED_RID, $roles)) {
      // Authenticated role is not saved in DB.
      $context['sandbox']['uids'] = db_select('users', 'u')
                                      ->fields('u', array('uid'))
                                      ->condition('u.uid', 0,'<>')
                                      ->execute()
                                      ->fetchCol();
    }
    else {
      $context['sandbox']['uids'] = db_select('users_roles', 'ur')
                                      ->distinct()
                                      ->fields('ur', array('uid'))
                                      ->condition('rid', $roles ,'IN')
                                      ->execute()
                                      ->fetchCol();
    }

    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($context['sandbox']['uids']);
  }

  $limit = 5;

  // With each pass through the callback, retrieve the next group of uids.
  $round_uids = array_slice($context['sandbox']['uids'], 0, $limit);
  $context['sandbox']['uids'] = array_diff($context['sandbox']['uids'], $round_uids);

  $accounts = user_load_multiple($round_uids);
  $ldap_user_conf = ldap_user_conf();
  foreach ($accounts as $account) {
    $ldap_user_conf->provisionLdapEntry($account);

    $context['results'][] = $account->name;

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['message'] = t('Now provisioning %user', array('%user' => $account->name));
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch 'finished' callback.
 */
function _ldap_bulk_provisioning_drupal_to_ldap_finished($success, $results, $operations) {
  if ($success) {
    $results = isset($results) ? $results : array();

    $message = t('!count accounts provisioned.', array('!count' => count($results)));
    $message .= theme('item_list', array('items' => $results));
    drupal_set_message(filter_xss($message));
  }
  else {
    // An error occurred.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE))
    );
    drupal_set_message($message, 'error');
  }
}
