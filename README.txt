CONTENTS OF THIS FILE
---------------------
  
* Introduction
* Requirements
* Configuration
* Maintainers


INTRODUCTION
------------

This module adds a bulk provisioning (currently only Drupal -> LDAP) by user
roles functionality.

* For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/haggins/2414219

* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2414219


REQUIREMENTS
------------
This module requires the following modules:

* LDAP (https://www.drupal.org/project/ldap)


CONFIGURATION
-------------
Visit admin/config/people/ldap/user/bulk_d2l and select the users roles to be
provisioned.


MAINTAINERS
-----------

Current maintainers:
* Patrick Stürmlinger (haggins) - https://www.drupal.org/u/haggins

This project has been sponsored by:
* Berger Schmidt - Praktische Informatik
   Specialized in consulting and planning of Drupal powered sites,
   BERGER SCHMIDT offers installation, development, theming and customization
   to get you started. Visit http://www.berger-schmidt.de for more
   information.
